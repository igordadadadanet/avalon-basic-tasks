import { 
  getTotal,
  getError,
  getHeroes,
  getFullName,
  getNewArray,
  getMinValue, 
  getOddValues, 
  get2MinValues, 
  getUniqueValues, 
  getStringLengths,
  getCountOfVowels, 
  getMinValuesFromRows, 
} from '../src/index.js';


describe('Avalon basic tasks', () => {
  describe('Task 1 - getOddValue', () => {
    it('Function is defined', () => {
      expect(getOddValues).toBeDefined();
    });
    
    it.each`
      arr          | expected
      ${[]}        | ${[]}
      ${[0]}       | ${[]}
      ${[2,6,8]}   | ${[]}
      ${[1,2,3,4]} | ${[1,3]}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getOddValues(arr)).toEqual(expected);
    });
  });

  describe('Task 2 - getMinValue', () => {
    it('Function is defined', () => {
      expect(getMinValue).toBeDefined();
    });
    
    it.each`
      arr           | expected
      ${[-7]}       | ${-7}
      ${[2,6,8]}    | ${2}
      ${[1,2,3,-4]} | ${-4}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getMinValue(arr)).toEqual(expected);
    });
  });

  describe('Task 3 - getMinValuesFromRows', () => {
    it('Function is defined', () => {
      expect(getMinValuesFromRows).toBeDefined();
    });
    
    it.each`
      arr                          | expected
      ${[[2,3,4,-5],[0,5,1,7]]}    | ${[-5,0]}
      ${[[1,2,3,4],[1,2,3,4,10]]}  | ${[1,1]}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getMinValuesFromRows(arr).sort((a, b) => a - b)).toEqual(expected);
    });
  });

  describe('Task 4 - get2MinValues', () => {
    it('Function is defined', () => {
      expect(get2MinValues).toBeDefined();
    });
    
    it.each`
      arr              | expected
      ${[1,1,1,1]}     | ${[1,1]}
      ${[-5,2,3,4]}    | ${[-5,2]}
      ${[4,3,2,1,10]}  | ${[1,2]}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(get2MinValues(arr).sort((a, b) => a - b)).toEqual(expected);
    });
  });

  describe('Task 5 - getCountOfVowels', () => {
    it('Function is defined', () => {
      expect(getCountOfVowels).toBeDefined();
    });
    
    it.each`
      str              | expected
      ${'Rrrrhhhhh'}                                                | ${0}
      ${'Return the number (count) of vowels in the given string.'} | ${15}
    `('$str => $expected', ({ str, expected }) => {
      expect(getCountOfVowels(str)).toEqual(expected);
    });
  });

  describe('Task 6 - getUniqueValues', () => {
    it('Function is defined', () => {
      expect(getUniqueValues).toBeDefined();
    });
    
    it.each`
      arr              | expected
      ${[1,1,1]}       | ${[1]}
      ${[0,1,2,3]}     | ${[0,1,2,3]}
      ${[1,2,2,4,5,5]} | ${[1,2,4,5]}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getUniqueValues(arr)).toEqual(expected);
    });
  });

  describe('Task 7 - getStringLengths', () => {
    it('Function is defined', () => {
      expect(getStringLengths).toBeDefined();
    });
    
    it.each`
      arr                                 | expected
      ${['Есть', 'жизнь', 'на', 'Марсе']} | ${[4, 5, 2, 5]}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getStringLengths(arr)).toEqual(expected);
    });
  });

  describe('Task 8 - getTotal', () => {
    it('Function is defined', () => {
      expect(getTotal).toBeDefined();
    });
    it.each`
      arr                                 | expected
      ${[
        { price: 10, count: 2 }, 
        { price: 100, count: 1},
        { price: 2, count: 5},
        { price: 15, count: 6},
      ]}                                  | ${220}
      ${[
        { price: 0, count: 0 }, 
        { price: 0, count: 0},
      ]}                                  | ${0}
      ${[]}                               | ${0}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getTotal(arr)).toEqual(expected);
    });
  });

  describe('Task 9 - getError', () => {
    it('Function is defined', () => {
      expect(getError).toBeDefined();
    });
    it.each`
      arr     | expected
      ${500}  | ${'Ошибка сервера'}
      ${401}  | ${'Ошибка авторизации'}
      ${402}  | ${'Ошибка сервера'}
      ${403}  | ${'Доступ запрещен'}
      ${404}  | ${'Не найдено'}
      ${999}  | ${''}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getError(arr)).toEqual(expected);
    });
  });

  describe('Task 10 - getFullName', () => {
    it('Function is defined', () => {
      expect(getFullName).toBeDefined();
    });
    it.each`
      arr                                                                      | expected
      ${{ firstName: 'Петр', secondName: 'Васильев', patronymic: 'Иванович'}}  | ${'ФИО: Петр Иванович Васильев'}
      ${{ firstName: 'Q', secondName: 'W', patronymic: 'E'}}                   | ${'ФИО: Q E W'}
    `('$arr => $expected', ({ arr, expected }) => {
      expect(getFullName(arr)).toEqual(expected);
    });
  });

  describe('Task 11 - getNewArray', () => {
    it('Function is defined', () => {
      expect(getNewArray).toBeDefined();
    });
    it.each`
      arr          | mul  | expected
      ${[]}        | ${5} | ${[]}
      ${[1,2,3]}   | ${5} | ${[5,10,15]}
      ${[1,2,3]}   | ${1} | ${[1,2,3]}
    `('$arr => $expected', ({ arr, mul, expected }) => {
      expect(getNewArray(arr, mul)).toEqual(expected);
    });
  });

  describe('Task 12 - getHeroes', () => {
    it('Function is defined', () => {
      expect(getHeroes).toBeDefined();
    });
    it.each`
      arr                                       | franchise   | expected
      ${[
        {name: 'Batman', franchise: 'DC'},
        {name: 'Ironman', franchise: 'Marvel'},
        {name: 'Thor', franchise: 'Marvel'},
        {name: 'Superman', franchise: 'DC'},
      ]}                                        | ${'Marvel'} | ${'Ironman, Thor'}
      ${[
        {name: 'Batman', franchise: 'DC'},
        {name: 'Ironman', franchise: 'Marvel'},
        {name: 'Thor', franchise: 'Marvel'},
        {name: 'Superman', franchise: 'DC'},
      ]}                                        | ${'DC'} | ${'Batman, Superman'}
    `('$arr => $expected', ({ arr, franchise, expected }) => {
      expect(getHeroes(arr, franchise)).toEqual(expected);
    });
  });
});
